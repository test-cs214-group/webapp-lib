package cs214.webapp.server

import cs214.webapp.server.StateMachine
import cs214.webapp.server.utils.PackageLoader

import java.io.File
import java.net.{URI, URLDecoder}
import java.util.jar.{JarEntry, JarFile}
import scala.util.Try

class ApplicationJVM:

  def start(): Unit =

    val classes = PackageLoader.loadClasses()

    classes.foreach { candidateClass =>
      val isApp = classOf[StateMachine[_, _, _]].isAssignableFrom(candidateClass)

      if isApp then
        val appLogicInstance = candidateClass
          .getDeclaredConstructor()
          .newInstance().asInstanceOf[StateMachine[_, _, _]]

        if !WebServer.isRegistered(appLogicInstance) then
          WebServer.register(appLogicInstance)
    }

    WebServer.start()
