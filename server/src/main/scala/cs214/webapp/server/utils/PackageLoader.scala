package cs214.webapp.server.utils

import java.net.URLDecoder
import java.util.jar.{JarEntry, JarFile}
import scala.collection.mutable

object PackageLoader:

  private val CLASS_SUFFIX = ".class"

  def loadClasses(): List[Class[_]] =
    val classLoader = ClassLoader.getSystemClassLoader
    
    val classes: mutable.Set[Class[_]] = mutable.Set()
    
    for
      p <- classLoader.getDefinedPackages
    yield
      val packageName = p.getName
      val packagePath = packageName.replace('.', '/')
      val packageUrl = classLoader.getResource(packagePath)

      var jarFileName: String = null
      var jf: JarFile = null
      var jarEntries: java.util.Enumeration[JarEntry] = null

      // build jar file name, then loop through zipped entries
      jarFileName = URLDecoder.decode(packageUrl.getFile, "UTF-8")
      jarFileName = jarFileName.substring(CLASS_SUFFIX.length, jarFileName.indexOf("!"))
      jf = new JarFile(jarFileName)
      jarEntries = jf.entries
      while (jarEntries.hasMoreElements) {
        val entry = jarEntries.nextElement
        val entryName = entry.getName

        if (entryName.startsWith(packageName) && entryName.length > packageName.length + CLASS_SUFFIX.length) {
          if (entryName.endsWith(CLASS_SUFFIX)) {
            // removes the .class extension

            val cls = entryName.replace('/', '.').replace('\\', '.').replace(CLASS_SUFFIX, "")

            val candidateClass = try
              Some(Class.forName(cls))
            catch case _: NoClassDefFoundError => None
            
            if candidateClass.isDefined then classes.add(candidateClass.get)
   
          }
        }
      }
    classes.toList

