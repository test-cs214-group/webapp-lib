package cs214.webapp.server

import cs214.webapp.{Action, AppInfo, AppWire, ClockDrivenStateMachineEvent, Tick, TickEventFormat, UserEvent, UserId, WireFormat}
import ujson.Value

import scala.annotation.unused
import scala.util.Try

/** A state machine describes the core logic of an application.
  *
  * @tparam Event
  *   The type of the events that this application expects.
  * @tparam State
  *   The type of internal states of this application.
  * @tparam View
  *   The type of the views sent to the clients.
  */
abstract class StateMachine[Event, State, View]:
  /** An identifier for this app type. */
  val appInfo: cs214.webapp.AppInfo

  /** Provides serialization methods for events and views. */
  val wire: AppWire[Event, View]

  /** Initializes a new application. */
  def init(clients: Seq[UserId]): State

  /** Simulates a transition of the state machine.
    *
    * @param state
    *   The current [[State]] of the application.
    * @param userId
    *   The [[UserId]] of the user triggering the event
    * @param event
    *   The [[Event]] received
    * @return
    *   A sequence of commands to be sent to clients, or [[scala.util.Failure]]
    *   if the event is illegal given the current state..
    */
  def transition(state: State)(userId: UserId, event: Event): Try[Seq[Action[State]]]

  /** Projects an application state to produce a user-specific view.
    *
    * This function hides any details of the application's state that the user
    * should not have access to.
    *
    * @param state
    *   A [[State]] of the application.
    * @param userId
    *   The [[UserId]] of a user.
    * @return
    *   A view for user [[uid]].
    */
  def project(state: State)(userId: UserId): View

/**
 * Extend this class if you want your state machine to receive a [[Tick]] event in addition
 * to your own custom events. This event will be received every [[clockPeriodMs]] milliseconds
 *
 * @tparam Event
 *   The type of the events that this application expects.
 * @tparam State
 *   The type of internal states of this application.
 * @tparam View
 *   The type of the views sent to the clients.
 */
abstract class ClockDrivenStateMachine[Event <: UserEvent, State, View]
  extends StateMachine[ClockDrivenStateMachineEvent, State, View]:

  /**
   * The [[Tick]] event will be received by the state machine every [[clockPeriodMs]] milliseconds.
   */
  val clockPeriodMs: Int

  /** Provides serialization methods for events and views of a [[ClockDrivenStateMachine]]. */
  val clockDrivenWire: AppWire[Event, View]

  override object wire extends AppWire[ClockDrivenStateMachineEvent, View]:
    override object eventFormat extends WireFormat[ClockDrivenStateMachineEvent]:
      override def decode(json: Value): Try[ClockDrivenStateMachineEvent] =
        Try:
          val isClockEvent =
            json.objOpt
              .flatMap(_.get(ClockDrivenStateMachine.CLOCK_EVENT_HEADER))
              .exists(_.bool)

          if isClockEvent then TickEventFormat.decode(json.obj.get("tick").get).get
          else clockDrivenWire.eventFormat.decode(json).get

      override def encode(t: ClockDrivenStateMachineEvent): Value =
        throw IllegalStateException("This shouldn't be used. Please use the ScalApp's wire instead.")


    override val viewFormat: WireFormat[View] = clockDrivenWire.viewFormat
    
object ClockDrivenStateMachine:
  val CLOCK_EVENT_HEADER = "X-Clock"
  val CLOCK_EVENT_TICK_HEADER = "tick"
