package cs214.webapp
package server

import cats.*
import cats.effect.*
import cats.effect.unsafe.implicits.global
import com.comcast.ip4s.*
import io.circe.generic.auto.*
import io.circe.syntax.*
import org.http4s.*
import org.http4s.circe.*
import org.http4s.dsl.*
import org.http4s.dsl.io.*
import org.http4s.ember.server.EmberServerBuilder
import org.http4s.headers.*
import org.http4s.implicits.*
import org.http4s.server.Router
import org.http4s.server.middleware.{CORS, ErrorAction, ErrorHandling}
import org.http4s.server.staticcontent.*
import ujson.Value

import java.io.FileNotFoundException
import java.net.InetAddress
import java.nio.charset.MalformedInputException
import java.util.concurrent.atomic.AtomicBoolean
import scala.collection.immutable.Map
import scala.collection.{concurrent, immutable, mutable}
import scala.jdk.CollectionConverters.*
import scala.util.{Failure, Success, Try}

object WebServer:
  import scala.collection.immutable.Map

  private case class RunningClock(
    clockId: UserId,
    running: AtomicBoolean
  )

  private case class RunningServerAppInstance(
    appInstance: ServerAppInstance,
    connectedUsersCount: Int
  )

  private val maxAppDescriptionLength = 160
  private val maxAppNameLength = 32

  private val appDirectory: mutable.Map[String, ServerApp] = mutable.Map()
  private val apps: concurrent.Map[AppInstanceId, RunningServerAppInstance] = concurrent.TrieMap()

  private val clocks: concurrent.Map[AppInstanceId, RunningClock] = concurrent.TrieMap()

  def isRegistered(smInstance: StateMachine[_, _, _]): Boolean =
    appDirectory.contains(smInstance.appInfo.id)

  /** Registers an arbitrary app. */
  def register(app: ServerApp): Unit =
    val appId = app.appInfo.id
    println(f"Registered $appId")

    require(
      app.appInfo.name.length <= maxAppNameLength,
      s"App name \"${app.appInfo.name}\" is too long (> $maxAppNameLength chars.)")
    require(
      app.appInfo.description.length <= maxAppDescriptionLength,
      s"App description \"${app.appInfo.description}\" is too long (> $maxAppDescriptionLength chars.)")

    this.appDirectory.put(appId, app)

  /** Registers an app by calling [[package]].register() */
  def register(packageName: String): Unit =
    Class.forName(f"$packageName.register").getDeclaredConstructor().newInstance()

  /** Registers a state-machine based app. */
  def register[E, V, S](sm: StateMachine[E, V, S]): Unit =
    register(StateMachineServerApp(sm))

  /** Paths where the static content served by the server is stored */
  private val WEB_SRC_PATH = "www/static/"

  /** Path to the html page to serve when accessing the server "/" path */
  private val HTML_APP_PATH = WEB_SRC_PATH + "webapp.html"

  val debug = false

  private def initializeApp(name: String, clients: Seq[UserId]): AppInstanceId =
    val appId = java.util.UUID.randomUUID.toString
    println(f"Creating instance of $name with appId = $appId")
    apps(appId) = RunningServerAppInstance(appDirectory(name).init(clients), 0)

    apps(appId).appInstance match
      case StateMachineServerAppInstance(sm: ClockDrivenStateMachine[_, _, _], _, _) =>
        startClockFor(appId, sm.clockPeriodMs)
      case _ => ()

    websocketServer.initializeApp(appId)
    appId

  private def shutdownApp(appId: AppInstanceId): Unit =
    apps.remove(appId)
    clocks.get(appId).map(_._2).foreach(_.set(false))
    clocks.remove(appId)

    println(s"shutdownApp($appId)")

  private def startClockFor(appId: AppInstanceId, clockResolutionMs: Int): Unit =
    object Clock extends Thread:
      override def run(): Unit =
        val clockName = s"clock-$appId"
        val running = AtomicBoolean(true)
        clocks.put(appId, RunningClock(clockName, running))
        setName(clockName)
        while running.get() do
          handleMessage(
            appId, "clock",
            ujson.Obj(
              ClockDrivenStateMachine.CLOCK_EVENT_HEADER -> true,
              ClockDrivenStateMachine.CLOCK_EVENT_TICK_HEADER -> TickEventFormat.encode(Tick(System.currentTimeMillis))
            )
          )
          Thread.sleep(clockResolutionMs)

        println(s"Clock $clockName stopped.")

    Clock.start()

  /** Creates a transition from the current state of the application to the next
    * state of the application.
    *
    * Given a [[Seq]] of [[UserId]], corresponding to the clients connected to
    * the app, accepts and event and the event's author's id. The [[apps]] is
    * then used to handle the event and gives a sequence of actions for each
    * user.
    *
    * @param clients
    *   The set of clients currently connected in the application.
    * @param uid
    *   The user id of the user who triggered the event.
    * @param event
    *   The event triggered by the user.
    * @return
    *   A map which links each user to a queue of views they should receive.
    */
  private def transition(
      clients: Seq[UserId],
      appId: AppInstanceId
  )(uid: UserId, event: ujson.Value): Try[immutable.Map[UserId, Seq[Action[ujson.Value]]]] =
    val instance = apps(appId).appInstance
    instance.synchronized {
      instance.transition(clients)(uid, event).map {
        case (views, newApp) =>
          apps.update(appId, apps(appId).copy(appInstance = newApp))
          views
      }
    }

  private def handleMessage(appId: AppInstanceId, uid: UserId, msg: Value): Unit =
    // On an event received by the client, use the transition function on the app instance
    // the client is connected to.
    val transitionResult = transition(websocketServer.connectedClients(appId), appId)(uid, msg)
    val serverResponse = transitionResult match
      case Failure(exception) =>
        val senderIsClock = clocks.exists((clockId, _) => clockId == uid)
        val recipients =
          if senderIsClock then websocketServer.connectedClients(appId)
          else Seq(uid)

        for recipient <- recipients do
          websocketServer.send(appId, recipient):
            EventResponse.Wire.encode(Failure(exception))
      case Success(userActions) =>
        for (uid, actions) <- userActions do
          websocketServer.send(appId, uid):
            EventResponse.Wire.encode(Success(actions))

  /** The websocket server */
  private lazy val websocketServer: WebsocketsCollection = new WebsocketsCollection(Config.WS_PORT):

    override def onMessageReceived(appId: AppInstanceId, uid: UserId, msg: ujson.Value): Unit =
      handleMessage(appId, uid, msg)

    // When the user joins the app, the projection of the current state is sent to them
    override def onClientConnect(appId: AppInstanceId, uid: UserId): Unit =
      apps(appId).copy(connectedUsersCount = apps(appId).connectedUsersCount+1)

      val js = apps(appId).appInstance.respondToNewClient(uid)

      println(f"onClientConnect($appId, $uid)")
      websocketServer.send(appId, uid):
        EventResponse.Wire.encode(Success(List(Action.Render(js))))

    override def onClientDisconnect(appId: AppInstanceId, uid: UserId): Unit =
      apps(appId).copy(connectedUsersCount = apps(appId).connectedUsersCount - 1)
      println(f"onClientDisconnect($appId, $uid)")

      if apps(appId).connectedUsersCount == 0 then
        println(s"$appId has not registered users left.")
        shutdownApp(appId)

  private object HttpServer extends IOApp:
    private val dsl = Http4sDsl[IO]

    /** Reads a static file given its path on the file system, from the resource
      * directory in the classpath
      * @param filePath
      *   The path of the file to read, contained in the classpath's /resources
      *   directory.
      * @return
      *   The content of the file.
      */
    private def readStatic(filePath: String): Either[String, String] =
      try
        val source = scala.io.Source.fromResource(filePath)
        try Right(source.mkString)
        finally source.close()
      catch
        case e: FileNotFoundException => Left(e.getMessage)

    def staticRoutes(systemPrefix: String) = HttpRoutes.of[IO] {
      case req @ GET -> Root / path =>
        val systemPath = systemPrefix + path
        StaticFile.fromResource("/" + systemPath, Some(req)).getOrElseF(NotFound(systemPath))
    }

    extension (req: Request[IO])
      def decodeWithWire[T](wire: WireFormat[T]): IO[T] =
        req.as[String].flatMap: body =>
          IO.fromTry(wire.decode(ujson.read(body)))

    lazy val hostAddress: String =
      val addresses =
        for
          intf <- java.net.NetworkInterface.getNetworkInterfaces.asScala
          if intf.isUp
          _ = println(f"Found interface $intf")
          addr <- intf.getInetAddresses.asScala
          if (addr.isInstanceOf[java.net.Inet4Address]
            && !addr.isLinkLocalAddress
            && !addr.isLoopbackAddress)
          _ = println(f"Found address ${addr.getHostAddress}")
        yield addr.getHostAddress
      Try(addresses.toList.head).getOrElse(InetAddress.getLocalHost.getHostAddress)

    private val httpApp: HttpApp[IO] =
      val api = HttpRoutes.of[IO] {
        // Gives the ip address of the machine on which this server is running
        case GET -> Root / Endpoints.Api.ip.path =>
          Ok(hostAddress)
        // Lists the different apps available for the server
        case GET -> Root / Endpoints.Api.listApps.path =>
          Ok(ListAppsResponse.Wire.encode(ListAppsResponse(appDirectory.values.map(_.appInfo).toSeq)).toString)
        // Lists users registered in a given app
        case req @ GET -> Root / appInstanceId / Endpoints.Api.appInfo.path =>
          if apps.contains(appInstanceId) then
            val app = apps(appInstanceId).appInstance
            val shareUrl = f"http://$hostAddress:${Config.HTTP_PORT}/app/${app.appInfo.id}/$appInstanceId/"
            val wsEndpoint = f"ws://{{hostName}}:${Config.WS_PORT}/$appInstanceId/{{userId}}"
            val response = AppInfoResponse(appInstanceId, app.registeredUsers, wsEndpoint, shareUrl)
            Ok(AppInfoResponse.Wire.encode(response).toString)
          else
            BadRequest(f"Unrecognized id $appInstanceId")
        // Returns a generated appId the player can use to connect
        case req @ POST -> Root / Endpoints.Api.initializeApp.path =>
          req.decodeWithWire(InitializeAppRequest.Wire).flatMap(body =>
            val appId = initializeApp(body.appName, body.userIds)
            Ok(InitializeAppResponse.Wire.encode(InitializeAppResponse(appId)).toString)
          )
      }

      val pages = HttpRoutes.of[IO] {
        case GET -> _ => // All routing happens client-side
          Ok(
            readStatic(HTML_APP_PATH).getOrElse(throw new IllegalStateException(s"Could not find $HTML_APP_PATH")),
            `Content-Type`(MediaType.text.html)
          )
      }

      Router(
        "/api" -> api,
        "/static" -> staticRoutes(WEB_SRC_PATH),
        "/" -> pages
      ).orNotFound

    def printBacktrace(exn: Throwable): IO[Unit] =
      IO.println(exn) >> {
        val sw = java.io.StringWriter()
        exn.printStackTrace(java.io.PrintWriter(sw));
        IO.println(sw.toString)
      }

    private val withErrorLogging = ErrorHandling.Recover.total(
      ErrorAction.log(
        httpApp,
        messageFailureLogAction = (t, _) => printBacktrace(t) >> IO.raiseError(t),
        serviceErrorLogAction = (t, _) => printBacktrace(t) >> IO.raiseError(t)
      )
    )

    private val withCors = CORS.policy
      .withAllowOriginAll(withErrorLogging)

    override def run(args: List[String]): IO[ExitCode] =
      val ipAddress = ipv4"0.0.0.0"
      val port = Port.fromInt(Config.HTTP_PORT).get
      val server = EmberServerBuilder
        .default[IO]
        .withHost(ipAddress)
        .withPort(port)
        .withHttpApp(withCors)
        .build
        .use(_ => IO.never)
        .as(ExitCode.Success)
      println(f"HTTP Server started on $ipAddress:$port.")
      server

  def start(): Unit =
    websocketServer.run()
    HttpServer.run(List.empty).unsafeRunSync()
