package cs214.webapp

import ujson.Value

import scala.util.Try

case class AppInfo(
  /** A url-compatible app identifier (for example "tictactoe"),  */
  id: String,
  /** The display name of the application (for example "Tic Tac Toe"), less than 32 characters */
  name: String,
  /** A description of the application, less than 128 characters */
  description: String,
  /** The current year for the course */
  year: Int,
)


/** The ServerApp interface is used by the server to create new webapp
  * instances.
  */
trait ServerApp:
  def appInfo: AppInfo
  def init(clients: Seq[UserId]): ServerAppInstance

/** Provides all the necessary encoding/decoding methods for an application. */
trait AppWire[Event, View]:
  val eventFormat: WireFormat[Event]
  val viewFormat: WireFormat[View]

/** The ServerAppInstance interface is used by the server to store the state of
  * an app.
  *
  * Note that this interface is *pure*: the [[transition]] function creates a
  * new ServerApp.
  */
trait ServerAppInstance:
  val appInfo: AppInfo
  val registeredUsers: Seq[String]

  /** Compute setup actions to send to client. */
  def respondToNewClient(userId: UserId): ujson.Value

  /** Simulate one step of the underlying state machine. */
  def transition(clients: Seq[UserId])(
      userId: UserId,
      jsEvent: Value
  ): Try[(Map[UserId, Seq[Action[Value]]], ServerAppInstance)]


trait ClockDrivenStateMachineEvent

case class Tick(systemMillis: Long) extends ClockDrivenStateMachineEvent

trait UserEvent extends ClockDrivenStateMachineEvent

object TickEventFormat extends WireFormat[Tick]:
  override def decode(json: Value): Try[Tick] =
    Try:
      Tick(json.obj.get("millis").get.num.toLong)

  override def encode(t: Tick): Value = ujson.Obj("type" -> "Tick", "millis" -> ujson.Num(t.systemMillis.toDouble))
