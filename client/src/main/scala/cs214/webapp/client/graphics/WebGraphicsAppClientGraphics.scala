package cs214.webapp.client.graphics

import cs214.webapp.UserId
import cs214.webapp.client.StateMachineClientAppInstance
import org.scalajs.dom

abstract class WebGraphicsAppClientGraphics[Event, View](
  userId: UserId,
  sendMessage: ujson.Value => Unit,
  target: dom.Element
) extends StateMachineClientAppInstance[Event, View](userId, sendMessage, target)
