package cs214.webapp.client.graphics

import cs214.webapp.UserId
import cs214.webapp.client.StateMachineClientAppInstance
import org.scalajs.dom
import org.scalajs.dom.{CanvasFillRule, CanvasGradient, CanvasPattern, CanvasRenderingContext2D, HTMLElement, ImageData, MouseEvent, Path2D, ResizeObserver, ResizeObserverEntry, TextMetrics}
import scalatags.JsDom
import scalatags.JsDom.all.*

import scala.scalajs.js

abstract class PainterAppClientGraphics[Event, View](
    userId: UserId,
    sendMessage: ujson.Value => Unit,
    target: dom.Element
) extends StateMachineClientAppInstance[Event, View](userId, sendMessage, target):

  trait CanvasEvent

  enum MouseButton:
    case Left
    case Middle
    case Right

  object MouseButton:
    def fromInt(htmlButtonIdentifier: Int): MouseButton =
      htmlButtonIdentifier match
        case 0 => Left
        case 1 => Middle
        case 2 => Right
        case i => throw IllegalStateException(s"Unsupported mouse button: $i")

  enum MouseEvent extends CanvasEvent:
    case MouseDown(x: Double, y: Double, button: MouseButton)
    case MouseUp(x: Double, y: Double, button: MouseButton)
    case MouseClick(x: Double, y: Double, button: MouseButton)
    case MouseMoveTo(x: Double, y: Double)

  enum KeyboardEvent extends CanvasEvent:
    case KeyDown(keyCode: Int)
    case KeyUp(keyCode: Int)

  private val canvasDim = 400

  private val initialFontSize = 9.0
  private var currentFontSize = initialFontSize

  private var ctx: Option[CanvasRenderingContext2D] = None
  private var canvas: Option[dom.HTMLCanvasElement] = None

  private var currView: Option[View] = None

  private val canvasFrag: JsDom.all.Frag =
    this.canvas = Some(
      JsDom.all.canvas(
        border := "solid 1px black",
        width := canvasDim,
        height := canvasDim,
        onclick := ((mouseEvent: dom.MouseEvent) => handleMouseEvent(mouseEvent, "click")),
        onmousedown := ((mouseEvent: dom.MouseEvent) => handleMouseEvent(mouseEvent, "down")),
        onmouseup := ((mouseEvent: dom.MouseEvent) => handleMouseEvent(mouseEvent, "up")),

      ).render
    )

    dom.window.addEventListener("keydown", (e: dom.KeyboardEvent) => handleKeyEvent(e, "keydown"))
    dom.window.addEventListener("keyup", (e: dom.KeyboardEvent) => handleKeyEvent(e, "keyup"))

    this.ctx = Some(this.canvas.get.getContext("2d").asInstanceOf[CanvasRenderingContext2D])
    this.canvas

  def handleCanvasEvent(view: View, canvasEvent: CanvasEvent): Option[Event]

  def draw(userId: UserId, view: View, ctx: CanvasRenderingContext2D): Unit

  def fillText(text: String, x: Double, y: Double, maxWidth: Double = 99): Unit =
    val context = ctx.getOrElse(throw new IllegalStateException("The 2d context should have been initialized"))

    context.setTransform(
      1, 0,
      0, 1,
      0, 0
    )

    context.fillText(
      text,
      x * this.canvas.get.width,
      y * this.canvas.get.height,
      maxWidth * this.canvas.get.width
    )

    context.setTransform(
      this.canvas.get.width, 0,
      0, this.canvas.get.height,
      0, 0
    )

  def getFontSize: Double = currentFontSize / this.canvas.get.height

  def withFontSizeScaledBy(fontSizeScaleFactor: Double)(draw: => Unit): Unit =
    val context = ctx.getOrElse(throw new IllegalStateException("The 2d context should have been initialized"))

    currentFontSize = initialFontSize * fontSizeScaleFactor
    context.font = s"${currentFontSize}px sans-serif"

    require(currentFontSize >= 1, "Invalid font size: cannot have a font size smaller than 1px.")

    draw
    currentFontSize = initialFontSize
    context.font = s"${currentFontSize}px sans-serif"

  override def render(userId: UserId, view: View): JsDom.all.Frag =
    val context = ctx.getOrElse(throw new IllegalStateException("The 2d context should have been initialized"))

    context.clearRect(0, 0, this.canvas.get.width, this.canvas.get.height)
    context.save()

    context.setTransform(
      this.canvas.get.width,  0,
      0,          this.canvas.get.height,
      0,          0
    )

    context.font = s"${initialFontSize}px sans-serif"
    context.lineWidth = 1/canvasDim.toDouble

    currView = Some(view)
    // TODO Lookup how to load images
    // TODO Add mouse up; mouse down; click. Override empty functions. Add keys on window
    draw(userId, view, context)

    context.restore()

    div(
      width := "100%",
      style := "display: flex; flex-direction: column; justify-content: center; align-items: center;",
      canvasFrag
    )

  private def handleKeyEvent(evt: dom.KeyboardEvent, evtType: "keydown"|"keyup"): Unit =
    val event = evtType match
      case "keydown" => KeyboardEvent.KeyDown(evt.keyCode)
      case "keyup" => KeyboardEvent.KeyUp(evt.keyCode)

    currView.flatMap(handleCanvasEvent(_, event)).foreach(sendEvent)

  private def handleMouseEvent(evt: dom.MouseEvent, evtType: "down"|"up"|"click"): Unit =
    val rect = canvas.get.getBoundingClientRect();

    val x = evt.clientX - rect.left
    val y = evt.clientY - rect.top

    currView
      .flatMap { view =>
        val canvasX = x / this.canvas.get.width
        val canvasY = y / this.canvas.get.height

        val event = evtType match
          case "down" => MouseEvent.MouseDown(canvasX, canvasY, MouseButton.fromInt(evt.button))
          case "click" => MouseEvent.MouseClick(canvasX, canvasY, MouseButton.fromInt(evt.button))
          case "up" => MouseEvent.MouseUp(canvasX, canvasY, MouseButton.fromInt(evt.button))

        handleCanvasEvent(view, event)
      }
      .foreach(sendEvent)
