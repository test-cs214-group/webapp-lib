package cs214.webapp.client.graphics

import cs214.webapp.UserId
import cs214.webapp.client.StateMachineClientAppInstance
import org.scalajs.dom
import org.scalajs.dom.HTMLInputElement
import scalatags.JsDom
import scalatags.JsDom.all.*


abstract class TextBasedAdventureAppClientGraphics[Event, View](
    userId: UserId,
    sendMessage: ujson.Value => Unit,
    target: dom.Element
) extends StateMachineClientAppInstance[Event, View](userId, sendMessage, target):

  enum TextSegmentStyle:
    case BackgroundColor(color: String)
    case Color(color: String)
    case CSSProperty(name: String, value: String)

    def css: String =
      this match
        case TextSegmentStyle.BackgroundColor(color) => s"background-color: $color"
        case TextSegmentStyle.Color(color) => s"color: $color"
        case TextSegmentStyle.CSSProperty(name, value) => s"$name: $value"

  enum MouseButton:
    case Left
    case Middle
    case Right

  object MouseButton:
    def from(htmlButtonIdentifier: Int): MouseButton =
      htmlButtonIdentifier match
        case 0 => Left
        case 1 => Middle
        case 2 => Right
        case i => throw IllegalStateException(s"Unsupported mouse button: $i")

  enum MouseEvent:
    case Click(button: MouseButton)
    case MouseDown(button: MouseButton)
    case MouseUp(button: MouseButton)
    case HoverEnter
    case HoverLeave

  case class TextSegment(
    text: String,
    styleKey: Option[String] = None,
    inlineStyle: Set[TextSegmentStyle] = Set(),
    onMouseEvent: MouseEvent => Unit = _ => ()
  )

  /** Transforms a text input from the user into an app [[Event]]
   * @param text The input text typed by the user
   * @return An application event
   */
  def handleTextInput(text: String): Unit

  // TODO Document the segmentation of the console using [[TextSegment]]
  /** Transforms a view received by the server into a console display.
   *
   * @param userId The id of the user using this client
   * @param view The view of the application received by the server
   * @return
   */
  def renderView(userId: UserId, view: View): Vector[TextSegment]

  def styleLookupTable: Map[String, Set[TextSegmentStyle]] = Map()

  private def focusConsoleInput(): Unit =
    dom.document.getElementById("console-input").asInstanceOf[HTMLInputElement].focus()

  override def render(userId: UserId, view: View): JsDom.all.Frag =
    frag(
      div(
        `class` := "textbasedadventure",
        color := "green",
        backgroundColor := "black",
        div(
          onclick := (() => focusConsoleInput()),
          `class` := "console",
          pre(
            for textSegment <- renderView(userId, view) yield
              span(
                textSegment.text, margin := "0", height := "1em",
                style :=
                  textSegment.styleKey.map(key => styleLookupTable(key).map(_.css).mkString(";")).getOrElse("")
                  + ";" +
                  textSegment.inlineStyle.map(_.css).mkString(";"),
                onclick := {
                  (e: dom.MouseEvent) =>
                    textSegment.onMouseEvent(MouseEvent.Click(MouseButton.from(e.button)))
                },
                onmousedown := {
                  (e: dom.MouseEvent) => textSegment.onMouseEvent(MouseEvent.MouseDown(MouseButton.from(e.button)))},
                onmouseup := {(e: dom.MouseEvent) => textSegment.onMouseEvent(MouseEvent.MouseUp(MouseButton.from(e.button)))},
                onmouseover := {(_: dom.MouseEvent) => textSegment.onMouseEvent(MouseEvent.HoverEnter)},
                onmouseout := {(_: dom.MouseEvent) => textSegment.onMouseEvent(MouseEvent.HoverLeave)},
              )
          )
        ),
        input(
          `type` := "text",
          autofocus := true,
          id := "console-input",
          tabindex := 0,
          onkeydown := handleKeyDown,
          onblur := {
            dom.window.setTimeout(() => {
              focusConsoleInput()
            }, 0)
          }
        )
      )
    )

  private def handleKeyDown(e: dom.KeyboardEvent): Unit =
    if e.keyCode == dom.KeyCode.Enter then
      e.target match
        case inputElement: HTMLInputElement =>
          e.preventDefault()
          handleTextInput(inputElement.value)
